import requests
import json
from bs4 import BeautifulSoup
import urllib.request
import csv


#---- EXERCICE 1 ----#

def http_request():
    return requests.post(url="https://httpbin.org/anything",data={"isadmin": 1}).text


#---- EXERCICE 2 ----#

class Store:
    def __init__(self): 
        try:
            with open("./third_part/data/data.json", "r") as read_file:
                self.data = json.load(read_file)
        except:
            raise Exception("File not found or .gz not extracted.")

    def truncate_and_clean(self, name):
        bannedList = ['Months', 'months', 'kg']
        name_l = name.split(' ')
        for i in range(0, len(name_l)):
            if name_l[i].isdigit() or any (x in name_l[i] for x in bannedList):
                name = ' '.join(name_l[0:i])
        name = ' '.join(name_l)
        if len(name) > 30:
            name = name[0:30]
        return (name)

    def write_in_csv(self, string):
        file = open("Store.csv", "a")
        file.write(string)
        file.close()

    def print_products(self):
        cmpt = 0
        
        for i in range (0, len(self.data['Bundles'])):
            
            ProductTag = 'None'
            if 'Product' in self.data['Bundles'][i]:
                ProductTag = 'Product'
            elif 'Products' in self.data['Bundles'][i]:
                ProductTag = 'Products'
            
            if ProductTag == 'None':
                if 'Name' in self.data['Bundles'][i]:
                    self.write_in_csv(self.truncate_and_clean(self.data['Bundles'][i]['Name']) + " UNKNOWN ERROR, NO ID\n")
            else:
                for o in range(0, len(self.data['Bundles'][i][ProductTag])):
                    isAvailable = False
                    if 'IsAvailable' in self.data['Bundles'][i][ProductTag][o]:
                        self.write_in_csv("You can buy " + self.truncate_and_clean(self.data['Bundles'][i][ProductTag][o]['Name']) +
                        " at our store at ")
                        if 'Price' in self.data['Bundles'][i][ProductTag][o] and self.data['Bundles'][i][ProductTag][o]['Price'] is not None: 
                            self.write_in_csv(str(round(self.data['Bundles'][i][ProductTag][o]['Price'], 1)))
                        elif 'WasPrice' in self.data['Bundles'][i][ProductTag][o]:
                            self.write_in_csv(str(round(self.data['Bundles'][i][ProductTag][o]['WasPrice'], 1)))
                        self.write_in_csv('\n')
                        cmpt += 1
                        isAvailable = True
                
                if (isAvailable is not True):
                        self.write_in_csv(str(self.data['Bundles'][i][ProductTag][o]['Stockcode']) + ' ' + str(self.truncate_and_clean(self.data['Bundles'][i][ProductTag][o]['Name']) + '\n'))
        print(cmpt, "articles were on available.")

    def __del__(self):
        print('end')

f = Store()
f.print_products()



#---- EXERCICE 3 ----#


urlpage = 'https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas'

headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3"}


req = urllib.request.Request(url=urlpage, headers=headers)
page = urllib.request.urlopen(req).read()


soup = BeautifulSoup(page, 'html.parser')
print(soup)


table = soup.find('div', attrs={'class': 'ng-tns-c199-3 ng-trigger ng-trigger-staggerFadeInOut product-grid'})
print(table)
results = table.find_all('div' , attrs={'class': 'shelfProductTile-information'})
print('Number of results', len(results))


rows = []
rows.append(["Title", "PrixL", "PrixT"])
print(rows)


data = []
for result in results:
  data.append(result.find_all('div' , attrs={'class':'shelfProductTile-information'}))
  if len(data) == 0:
    continue
data    


PrixL=[]
Title=[]
PrixT=[]

data2 =[]

for i in range(len(data)):
  Title.append(data[i][0].find('a' , attrs={'class':'shelfProductTile-descriptionLink'}).getText())
  PrixL.append(data[i][0].find('div' , attrs={'class':'shelfProductTile-cupPrice ng-star-inserted'}).getText())
  prix=data.find_all('div' , attrs={'class':'price price--large ng-star-inserted'}).getText()
  PrixT.append((prix.find('span' , attrs={'class' :'price-dollars'})+','+prix.find('div' , attrs={'class' :'price-centsPer'})))  
  data2.append((Title[i] ,PrixL[i] ,PrixT[i]))


rows.append(data2)

with open('res.csv','w', newline='') as f_output:
  csv_output = csv.writer(f_output)
  csv_output.writerows(rows)
