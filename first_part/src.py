import collections

def exercise_one():
    for x in range(1, 100):
        three = not x % 3
        five = not x % 5
    
        if three and five :
            print("ThreeFive")
        elif three:
            print("Three")
        elif five:
            print("Five")
        else:
            print(x)


def exercice_two(n):
    a = str(n)
    b = []
    for i in range(len(a)):
        c = 1
        for j in range(i,len(a)):
            c *= int(a[j])
            if c in b:
                return 0
            b.append(c)
    return 1

def exercice_two(str_list):
    sum_result = 0
    if type(str_list) != list:
        return False
    for elmnt in str_list:
        if type(elmnt) is str and elmnt.replace("-", "").isnumeric():
            sum_result += int(elmnt)
    return sum_result



def exercice_four(str_to_test, anagrams_to_test):
    anagrams_result = []
    str_letters_num = collections.Counter(str_to_test)
    for anagram in anagrams_to_test:
        anagram_num = collections.Counter(anagram)
        if str_letters_num == anagram_num:
            anagrams_result.append(anagram)
    return anagrams_result
