import random
import inspect

def random_gen():
    return random.randint(10, 20)


def decorator_to_str(func):
    # todo exercise 2
    def str_wrapper(*args, **kwargs):
        return str(func(*args, **kwargs))

    return str_wrapper


@decorator_to_str
def add(a, b):
    return a + b


@decorator_to_str
def get_info(d):
    return d['info']


def ignore_exception(exception):
    def exception_decorator(func):
        def exc_wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception:
                pass

        return exc_wrapper
    return exception_decorator


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


@CacheDecorator()
def pow(a, b):
    return a * b

def test_cache_decorator():
    print(pow(1, 2))
    print(pow(2, 2))
    print(pow(3, 5))
    print(pow(2, 2))
    print(pow(1, 2))

# test_cache_decorator()
class MetaInherList(type):
    # todo exercise 5
    def __new__(cls, name, parents, attrs):
        new_parents = parents + (list,)
        return super(MetaInherList, cls).__new__(cls, name, new_parents, attrs)


class CheckProcessAttr(type):
    # todo exercise 5
    def __new__(cls, name, parents, attrs):
        for attr_name, attr_obj in attrs.items():
            if str(attr_name) == "process":
                a_signature = inspect.signature(attr_obj)
                parameters = list(a_signature.parameters)
                print(f'The class {name} contains a process method...')
                if len(parameters) == 3:
                    print(f'... with 3 arguments')
        return super(CheckProcessAttr, cls).__new__(cls, name, parents, attrs)


class Ex:
    x = 4

class ProcessClass(metaclass=CheckProcessAttr):

    def process(self, a, b):
        return a + b

class ForceToList(Ex, metaclass=MetaInherList):
    pass

ProcessClass()